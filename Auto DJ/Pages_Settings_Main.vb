﻿Public Class Pages_Settings_Main
    Private Sub Pages_Settings_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ListBox1.Items.Clear()
        ListBox1.Items.Add("Имя раздела: " & Main.pages(Main.pages_data).pages_settings(0))
        ListBox1.Items.Add("Тип раздела: " & Main.pages(Main.pages_data).pages_settings(1))
        ListBox1.Items.Add("Треков в разделе: " & Main.pages(Main.pages_data).tracks.Count)
        ListBox1.Items.Add("Номер раздела: " & Main.pages_data + 1)
        If Main.pages(Main.pages_data).pages_settings(2) = 0 Then
            ListBox1.Items.Add("Повтор треков в разделе: исключён")
        Else
            ListBox1.Items.Add("Повтор треков в разделе: не исключён")
        End If
        If Main.pages(Main.pages_data).pages_settings(3) = 0 Then
            ListBox1.Items.Add("Минимальное кол-во треков для воспроизведения: автоматически")
        ElseIf Main.pages(Main.pages_data).pages_settings(3) = 1 Then
            ListBox1.Items.Add("Минимальное кол-во треков для воспроизведения: всё")
        Else
            ListBox1.Items.Add("Минимальное кол-во треков для воспроизведения: своё: " & Main.pages(Main.pages_data).pages_settings(5))
        End If
        If Main.pages(Main.pages_data).pages_settings(4) = 0 Then
            ListBox1.Items.Add("Максимальное кол-во треков для воспроизведения: автоматически")
        ElseIf Main.pages(Main.pages_data).pages_settings(4) = 1 Then
            ListBox1.Items.Add("Максимальное кол-во треков для воспроизведения: всё")
        Else
            ListBox1.Items.Add("Максимальное кол-во треков для воспроизведения: своё: " & Main.pages(Main.pages_data).pages_settings(6))
        End If
        ListBox1.Items.Add("Интервал мужду воспроизведением треков: " & Main.pages(Main.pages_data).pages_settings(7))
        TextBox1.Text = Main.pages(Main.pages_data).pages_settings(0)
        mode = Main.pages(Main.pages_data).pages_settings(1)
        Label4.Text = mode
        ComboBox1.SelectedIndex = Main.pages(Main.pages_data).pages_settings(2)
        ComboBox2.SelectedIndex = Main.pages(Main.pages_data).pages_settings(3)
        ComboBox3.SelectedIndex = Main.pages(Main.pages_data).pages_settings(4)
        TextBox2.Text = Main.pages(Main.pages_data).pages_settings(5)
        TextBox3.Text = Main.pages(Main.pages_data).pages_settings(6)
        TextBox4.Text = Main.pages(Main.pages_data).pages_settings(7)
        If ComboBox2.SelectedIndex = 2 Then
            TextBox2.Enabled = True
        Else
            TextBox2.Enabled = False
        End If
        If ComboBox3.SelectedIndex = 2 Then
            TextBox3.Enabled = True
        Else
            TextBox3.Enabled = False
        End If
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Pages_Settings_TimeData.ShowDialog()
    End Sub
    Dim mode As String
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If mode = "P" Then
            mode = "S"
        Else
            mode = "P"
        End If
        Label4.Text = mode
    End Sub
    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        If ComboBox2.SelectedIndex = 2 Then
            TextBox2.Enabled = True
        Else
            TextBox2.Enabled = False
        End If
    End Sub
    Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox3.SelectedIndexChanged
        If ComboBox3.SelectedIndex = 2 Then
            TextBox3.Enabled = True
        Else
            TextBox3.Enabled = False
        End If
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            If TextBox1.Text.IndexOf("<") > -1 And TextBox1.Text.IndexOf(">") > -1 And TextBox1.Text.IndexOf("/") > -1 And TextBox1.Text.IndexOf("\") > -1 And TextBox1.Text.IndexOf("=") > -1 Then Error 1
            Main.pages(Main.pages_data).pages_settings(0) = TextBox1.Text
            Main.pages(Main.pages_data).pages_settings(1) = mode
            Main.pages(Main.pages_data).pages_settings(2) = ComboBox1.SelectedIndex
            Main.pages(Main.pages_data).pages_settings(3) = ComboBox2.SelectedIndex
            Main.pages(Main.pages_data).pages_settings(4) = ComboBox3.SelectedIndex
            If ComboBox2.SelectedIndex = 2 Then
                Main.pages(Main.pages_data).pages_settings(5) = CType(TextBox2.Text, Integer)
            End If
            If ComboBox3.SelectedIndex = 2 Then
                If CType(TextBox3.Text, Integer) <= 0 Then Error 1
                Main.pages(Main.pages_data).pages_settings(6) = CType(TextBox3.Text, Integer)
            End If
            Main.pages(Main.pages_data).pages_settings(7) = CType(TextBox4.Text, Integer)
            Me.Close()
        Catch
            MsgBox("Неверно заданы значения полей ввода!", , "Ошибка Настройки")
        End Try
    End Sub
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Try
            If TextBox1.Text.IndexOf("<") > -1 And TextBox1.Text.IndexOf(">") > -1 And TextBox1.Text.IndexOf("/") > -1 And TextBox1.Text.IndexOf("\") > -1 And TextBox1.Text.IndexOf("=") > -1 Then Error 1
            Main.pages(Main.pages_data).pages_settings(0) = TextBox1.Text
            Main.pages(Main.pages_data).pages_settings(1) = mode
            Main.pages(Main.pages_data).pages_settings(2) = ComboBox1.SelectedIndex
            Main.pages(Main.pages_data).pages_settings(3) = ComboBox2.SelectedIndex
            Main.pages(Main.pages_data).pages_settings(4) = ComboBox3.SelectedIndex
            If ComboBox2.SelectedIndex = 2 Then
                Main.pages(Main.pages_data).pages_settings(5) = CType(TextBox2.Text, Integer)
            End If
            If ComboBox3.SelectedIndex = 2 Then
                If CType(TextBox3.Text, Integer) <= 0 Then Error 1
                Main.pages(Main.pages_data).pages_settings(6) = CType(TextBox3.Text, Integer)
            End If
            Main.pages(Main.pages_data).pages_settings(7) = CType(TextBox4.Text, Integer)
            ListBox1.Items.Clear()
            ListBox1.Items.Add("Имя раздела: " & Main.pages(Main.pages_data).pages_settings(0))
            ListBox1.Items.Add("Тип раздела: " & Main.pages(Main.pages_data).pages_settings(1))
            ListBox1.Items.Add("Треков в разделе: " & Main.pages(Main.pages_data).tracks.Count)
            ListBox1.Items.Add("Номер раздела: " & Main.pages_data + 1)
            If Main.pages(Main.pages_data).pages_settings(2) = 0 Then
                ListBox1.Items.Add("Повтор треков в разделе: исключён")
            Else
                ListBox1.Items.Add("Повтор треков в разделе: не исключён")
            End If
            If Main.pages(Main.pages_data).pages_settings(3) = 0 Then
                ListBox1.Items.Add("Минимальное кол-во треков для воспроизведения: автоматически")
            ElseIf Main.pages(Main.pages_data).pages_settings(3) = 1 Then
                ListBox1.Items.Add("Минимальное кол-во треков для воспроизведения: всё")
            Else
                ListBox1.Items.Add("Минимальное кол-во треков для воспроизведения: своё: " & Main.pages(Main.pages_data).pages_settings(5))
            End If
            If Main.pages(Main.pages_data).pages_settings(4) = 0 Then
                ListBox1.Items.Add("Максимальное кол-во треков для воспроизведения: автоматически")
            ElseIf Main.pages(Main.pages_data).pages_settings(4) = 1 Then
                ListBox1.Items.Add("Максимальное кол-во треков для воспроизведения: всё")
            Else
                ListBox1.Items.Add("Максимальное кол-во треков для воспроизведения: своё: " & Main.pages(Main.pages_data).pages_settings(6))
            End If
            ListBox1.Items.Add("Интервал мужду воспроизведением треков: " & Main.pages(Main.pages_data).pages_settings(7))
            TextBox1.Text = Main.pages(Main.pages_data).pages_settings(0)
            mode = Main.pages(Main.pages_data).pages_settings(1)
            Label4.Text = mode
            ComboBox1.SelectedIndex = Main.pages(Main.pages_data).pages_settings(2)
            ComboBox2.SelectedIndex = Main.pages(Main.pages_data).pages_settings(3)
            ComboBox3.SelectedIndex = Main.pages(Main.pages_data).pages_settings(4)
            TextBox2.Text = Main.pages(Main.pages_data).pages_settings(5)
            TextBox3.Text = Main.pages(Main.pages_data).pages_settings(6)
            TextBox4.Text = Main.pages(Main.pages_data).pages_settings(7)
            If ComboBox2.SelectedIndex = 2 Then
                TextBox2.Enabled = True
            Else
                TextBox2.Enabled = False
            End If
            If ComboBox3.SelectedIndex = 2 Then
                TextBox3.Enabled = True
            Else
                TextBox3.Enabled = False
            End If
        Catch
            MsgBox("Неверно заданы значения полей ввода!", , "Ошибка Настройки")
        End Try
    End Sub
End Class
