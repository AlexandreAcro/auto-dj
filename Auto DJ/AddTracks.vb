﻿Public Class AddTracks
    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender_dat As System.Object, ByVal e As System.EventArgs) Handles ComboBox.SelectedIndexChanged
        Button_Add.Enabled = True
    End Sub
    Private Sub Button_Cancel_Click(ByVal sender_dat As System.Object, ByVal e As System.EventArgs) Handles Button_Cancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub
    Private Sub Button_Add_Click(ByVal sender_dat As System.Object, ByVal e As System.EventArgs) Handles Button_Add.Click
        Dim result As DialogResult
        If ComboBox.SelectedIndex = 2 Then
            result = FolderBrowser.ShowDialog
        ElseIf ComboBox.SelectedIndex = 1 Then
            result = FolderBrowser.ShowDialog
        ElseIf ComboBox.SelectedIndex = 0 Then
            result = OpenFile.ShowDialog
        End If
        If result = DialogResult.OK Then
            Me.DialogResult = result
            Me.Close()
        End If
    End Sub
    Private Sub AddTracks_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If ComboBox.SelectedIndex = -1 Then
            ComboBox.SelectedIndex = 0
        End If
    End Sub
End Class
