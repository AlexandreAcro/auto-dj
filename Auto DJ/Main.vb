﻿Public Class Main
    Dim Player As New WMPLib.WindowsMediaPlayer
    Public pages As New List(Of Page), pages_data As Integer
    Public Information() As String = {"Версия: 1.0 - pro", "Авторские права: Copyright © 2015", "Новый проэкт", 0, ""} 'Версия, Авторские права, Путь к проэкту, Сохранен, Пароль
    Private Sub Main_Load(ByVal sender_dat As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pages.Add(New Page)
        pages_data = 0
        pages(0).pages_settings(0) = "Безымянный"
        pages(0).pages_settings(1) = "P"
        pages(0).pages_settings(2) = 0 'Повтор треков в разделе
        pages(0).pages_settings(3) = 0 'Минимальное колличесво треков для воспроизведения Режим 0 - Автоматически, 1 - Все, 2 - Своё
        pages(0).pages_settings(4) = 0 'Максимальное колличесво треков для воспроизведения Режим 0 - Автоматически, 1 - Все, 2 - Своё
        pages(0).pages_settings(5) = 0 'Минимальное колличесво треков для воспроизведения Если включено 2
        pages(0).pages_settings(6) = 1 'Максимальное колличесво треков для воспроизведения Если включено 2
        pages(0).pages_settings(7) = 1 'Интервал в секундах между воспроизведением треков
        pages(0).pages_settings(8) = "" 'Временная метка
        pages(0).pages_settings(9) = False 'Исключить из порядкового воспроизведения разделов
        Renov_PagesView()
    End Sub
    Public Sub UpDateDisplay(ByVal display As Short, ByVal data As String)
        If display = 0 Then
            Label2.Text = "Тип: " & pages(pages_data).pages_settings(1)
            PagesDisplay.Text = "Раздел: " & pages_data + 1 & " / " & pages.Count
        ElseIf display = 1 Then
            TextBox.Text = data
        End If
    End Sub
    Private Sub ОбзорСтраницToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ОбзорСтраницToolStripMenuItem.Click
        Dim result As DialogResult = AddTracks.ShowDialog
        If result = DialogResult.OK Then
            If AddTracks.ComboBox.SelectedIndex = 2 Then
                Try
                    Dim files() As String = IO.Directory.GetFiles(AddTracks.FolderBrowser.SelectedPath, "*.mp3", IO.SearchOption.AllDirectories)
                    If files.Length > 0 Then
                        Dim time_dat As Integer = 0, stayed_position As Short = 0
                        For i As Integer = 0 To files.Length - 1
                            Dim is_double As Boolean = False
                            If stayed_position = 0 Then
                                For Each i1 As String In pages(pages_data).tracks
                                    If i1 = files(i) Then
                                        is_double = True
                                        Exit For
                                    End If
                                Next
                            End If
                            If is_double = True Then
                                Dim result1 As DialogResult = LoadFile_YesNo.ShowDialog
                                If result1 = DialogResult.OK Then
                                    pages(pages_data).tracks.Add(files(i))
                                    Tracks_View.Items.Add(Tracks_View.Items.Count + 1 & " - " & Mid(files(i), files(i).LastIndexOf("\") + 2, files(i).Length - files(i).LastIndexOf("\") - 5))
                                    time_dat += 1
                                    If LoadFile_YesNo.CheckBox1.Checked Then
                                        stayed_position = 2
                                    End If
                                Else
                                    If LoadFile_YesNo.CheckBox1.Checked Then
                                        stayed_position = 1
                                    End If
                                End If
                            Else
                                If stayed_position = 2 Or stayed_position = 0 Then
                                    pages(pages_data).tracks.Add(files(i))
                                    Tracks_View.Items.Add(Tracks_View.Items.Count + 1 & " - " & Mid(files(i), files(i).LastIndexOf("\") + 2, files(i).Length - files(i).LastIndexOf("\") - 5))
                                    time_dat += 1
                                End If
                            End If
                        Next
                        MsgBox("Добавлено треков: " & time_dat & " из " & files.Length, , "Информация")
                    Else
                        MsgBox("По пути '" & AddTracks.FolderBrowser.SelectedPath & "' не найдено файлов MP3!", , "Ошибка импорта")
                    End If
                Catch
                    MsgBox("Произошла ошибка во время импорта треков из '" & AddTracks.FolderBrowser.SelectedPath & "' - папка недоступна!", , "Ошибка импорта")
                End Try
            ElseIf AddTracks.ComboBox.SelectedIndex = 1 Then
                Try
                    Dim files() As String = IO.Directory.GetFiles(AddTracks.FolderBrowser.SelectedPath, "*.mp3", IO.SearchOption.TopDirectoryOnly)
                    If files.Length > 0 Then
                        Dim time_dat As Integer = 0, stayed_position As Short = 0
                        For i As Integer = 0 To files.Length - 1
                            Dim is_double As Boolean = False
                            If stayed_position = 0 Then
                                For Each i1 As String In pages(pages_data).tracks
                                    If i1 = files(i) Then
                                        is_double = True
                                        Exit For
                                    End If
                                Next
                            End If
                            If is_double = True Then
                                Dim result1 As DialogResult = LoadFile_YesNo.ShowDialog
                                If result1 = DialogResult.OK Then
                                    pages(pages_data).tracks.Add(files(i))
                                    Tracks_View.Items.Add(Tracks_View.Items.Count + 1 & " - " & Mid(files(i), files(i).LastIndexOf("\") + 2, files(i).Length - files(i).LastIndexOf("\") - 5))
                                    time_dat += 1
                                    If LoadFile_YesNo.CheckBox1.Checked Then
                                        stayed_position = 2
                                    End If
                                Else
                                    If LoadFile_YesNo.CheckBox1.Checked Then
                                        stayed_position = 1
                                    End If
                                End If
                            Else
                                If stayed_position = 2 Or stayed_position = 0 Then
                                    pages(pages_data).tracks.Add(files(i))
                                    Tracks_View.Items.Add(Tracks_View.Items.Count + 1 & " - " & Mid(files(i), files(i).LastIndexOf("\") + 2, files(i).Length - files(i).LastIndexOf("\") - 5))
                                    time_dat += 1
                                End If
                            End If
                        Next
                        MsgBox("Добавлено треков: " & time_dat & " из " & files.Length, , "Информация")
                    Else
                        MsgBox("По пути '" & AddTracks.FolderBrowser.SelectedPath & "' не найдено файлов MP3!", , "Ошибка импорта")
                    End If
                Catch ex As Exception
                    MsgBox("Произошла ошибка во время импорта треков из '" & AddTracks.FolderBrowser.SelectedPath & "' - папка недоступна!", , "Ошибка импорта")
                End Try
            ElseIf AddTracks.ComboBox.SelectedIndex = 0 Then
                Dim files() As String = AddTracks.OpenFile.FileNames
                If files.Length > 0 Then
                    Dim time_dat As Integer = 0, stayed_position As Short = 0
                    For i As Integer = 0 To files.Length - 1
                        Dim is_double As Boolean = False
                        If stayed_position = 0 Then
                            For Each i1 As String In pages(pages_data).tracks
                                If i1 = files(i) Then
                                    is_double = True
                                    Exit For
                                End If
                            Next
                        End If
                        If is_double = True Then
                            Dim result1 As DialogResult = LoadFile_YesNo.ShowDialog
                            If result1 = DialogResult.OK Then
                                pages(pages_data).tracks.Add(files(i))
                                Tracks_View.Items.Add(Tracks_View.Items.Count + 1 & " - " & Mid(files(i), files(i).LastIndexOf("\") + 2, files(i).Length - files(i).LastIndexOf("\") - 5))
                                time_dat += 1
                                If LoadFile_YesNo.CheckBox1.Checked Then
                                    stayed_position = 2
                                End If
                            Else
                                If LoadFile_YesNo.CheckBox1.Checked Then
                                    stayed_position = 1
                                End If
                            End If
                        Else
                            If stayed_position = 2 Or stayed_position = 0 Then
                                pages(pages_data).tracks.Add(files(i))
                                Tracks_View.Items.Add(Tracks_View.Items.Count + 1 & " - " & Mid(files(i), files(i).LastIndexOf("\") + 2, files(i).Length - files(i).LastIndexOf("\") - 5))
                                time_dat += 1
                            End If
                        End If
                    Next
                    MsgBox("Добавлено треков: " & time_dat & " из " & files.Length, , "Информация")
                Else
                    MsgBox("По пути '" & AddTracks.FolderBrowser.SelectedPath & "' не найдено файлов MP3!", , "Ошибка импорта")
                End If
            End If
            Pages_View.Items.Item(pages_data) = pages_data + 1 & " - Раздел: '" & pages(pages_data).pages_settings(0) & "', Тип раздела: " & pages(pages_data).pages_settings(1) & ", Треков: " & pages(pages_data).tracks.Count
        End If
    End Sub
    Private Sub РазделитьПроэктыToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles РазделитьПроэктыToolStripMenuItem.Click
        pages.Add(New Page)
        UpDateDisplay(0, 0)
        pages(pages.Count - 1).pages_settings(0) = "Безымянный"
        pages(pages.Count - 1).pages_settings(1) = "P"
        pages(pages.Count - 1).pages_settings(2) = 0
        pages(pages.Count - 1).pages_settings(3) = 0
        pages(pages.Count - 1).pages_settings(4) = 0
        pages(pages.Count - 1).pages_settings(5) = 1
        pages(pages.Count - 1).pages_settings(6) = 1
        pages(pages.Count - 1).pages_settings(7) = 1
        pages(pages.Count - 1).pages_settings(8) = ""
        pages(pages.Count - 1).pages_settings(9) = False
        Pages_View.Items.Add(pages.Count & " - Раздел: '" & pages(pages.Count - 1).pages_settings(0) & "', Тип раздела: " & pages(pages.Count - 1).pages_settings(1) & ", Треков: " & pages(pages.Count - 1).tracks.Count)
    End Sub
    Private Sub УдалитьТрекиToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles УдалитьТрекиToolStripMenuItem.Click
        If Tracks_View.SelectedIndices.Count > 0 Then
            Dim result As MsgBoxResult = MsgBox("Вы действительно хотите удалить " & Tracks_View.SelectedIndices.Count & " трек(ов)?", MsgBoxStyle.YesNo, "Удаление")
            Dim deleted As Integer = 0
            If result = MsgBoxResult.Yes Then
                Dim dat(Tracks_View.SelectedIndices.Count - 1) As String
                For i As Integer = 0 To Tracks_View.SelectedIndices.Count - 1
                    dat(i) = Tracks_View.SelectedIndices(i)
                Next
                For i As Integer = 0 To dat.Length - 1
                    pages(pages_data).tracks.RemoveAt(dat(i) - deleted)
                    Tracks_View.Items.RemoveAt(dat(i) - deleted)
                    deleted += 1
                Next
                Renov_TracksView()
            End If
            MsgBox("Удалено треков: " & deleted, , "Информация")
        Else
            MsgBox("Вы не выбрали треки для удаления! Треков: " & pages(pages_data).tracks.Count, , "Ошибка обозревателя")
        End If
    End Sub
    Private Sub УдалитьРаделToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles УдалитьРаделToolStripMenuItem.Click
        If Pages_View.SelectedIndices.Count > 0 Then
            Dim result As MsgBoxResult
            If Pages_View.SelectedIndices.Count > 1 Then
                Dim text_dat As String = ""
                For item As Integer = 0 To Pages_View.SelectedIndices.Count - 1
                    If item = 0 Then
                        text_dat = Pages_View.SelectedIndices.Item(item) + 1
                    Else
                        text_dat = text_dat & ", " & Pages_View.SelectedIndices.Item(item) + 1
                    End If
                Next
                result = MsgBox("Вы действительно хотите удалить разделы: '" & text_dat & " / " & pages.Count & "' ?", MsgBoxStyle.YesNo, "Удаление")
            Else
                result = MsgBox("Вы действительно хотите удалить раздел: '" & Pages_View.SelectedIndex + 1 & " / " & pages.Count & ", Имя раздела: '" & pages(Pages_View.SelectedIndex).pages_settings(0) & "', Тип раздела: " & pages(Pages_View.SelectedIndex).pages_settings(1) & ", Треков: " & pages(Pages_View.SelectedIndex).tracks.Count & "' ?", MsgBoxStyle.YesNo, "Удаление")
            End If
            Dim deleted As Integer = 0
            If result = MsgBoxResult.Yes Then
                Dim dat(Pages_View.SelectedIndices.Count - 1) As String
                For i As Integer = 0 To Pages_View.SelectedIndices.Count - 1
                    dat(i) = Pages_View.SelectedIndices(i)
                Next
                For i As Integer = 0 To dat.Length - 1
                    pages.RemoveAt(dat(i) - deleted)
                    deleted += 1
                Next
                If pages.Count = 0 Then
                    pages.Add(New Page)
                    UpDateDisplay(0, 0)
                    pages(pages.Count - 1).pages_settings(0) = "Безымянный"
                    pages(pages.Count - 1).pages_settings(1) = "P"
                    pages(pages.Count - 1).pages_settings(2) = 0
                    pages(pages.Count - 1).pages_settings(3) = 0
                    pages(pages.Count - 1).pages_settings(4) = 0
                    pages(pages.Count - 1).pages_settings(5) = 1
                    pages(pages.Count - 1).pages_settings(6) = 1
                    pages(pages.Count - 1).pages_settings(7) = 1
                    pages(pages.Count - 1).pages_settings(8) = ""
                    pages(pages.Count - 1).pages_settings(9) = False
                End If
                pages_data = 0
                Renov_PagesView()
                Renov_TracksView()
                UpDateDisplay(0, 0)
            End If
            MsgBox("Удалено разделов: " & deleted, , "Информация")
        Else
            MsgBox("Вы не выбрали разделы для удаления! Разделов: " & pages.Count, , "Ошибка обозревателя")
        End If
    End Sub
    Private Sub ОбзорСтраницToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ОбзорСтраницToolStripMenuItem1.Click
        If Pages_View.SelectedIndices.Count >= 2 Then
            Dim text_dat As String = ""
            For item As Integer = 0 To Pages_View.SelectedIndices.Count - 1
                If item = 0 Then
                    text_dat = Pages_View.SelectedIndices.Item(item) + 1
                Else
                    text_dat = text_dat & ", " & Pages_View.SelectedIndices.Item(item) + 1
                End If
            Next
            Dim result As MsgBoxResult = MsgBox("Вы действительно хотите объединить разделы: '" & text_dat & " / " & pages.Count & "' ?", MsgBoxStyle.YesNo, "Объединение")
            If result = MsgBoxResult.Yes Then
                Dim dat(Pages_View.SelectedIndices.Count - 1) As String
                For i As Integer = 0 To Pages_View.SelectedIndices.Count - 1
                    dat(i) = Pages_View.SelectedIndices(i)
                Next
                For i As Integer = 1 To dat.Length - 1
                    For Each i1 As String In pages(dat(i)).tracks
                        pages(dat(0)).tracks.Add(i1)
                    Next
                Next
                Dim deleted As Integer = 0
                For i As Integer = 1 To dat.Length - 1
                    pages.RemoveAt(dat(i) - deleted)
                    Pages_View.Items.RemoveAt(dat(i) - deleted)
                    deleted += 1
                Next
                Renov_PagesView()
                Renov_TracksView()
                MsgBox("Разделы: '" & text_dat & " / " & pages.Count & "' объединены успешно.", MsgBoxStyle.OkOnly, "Информация")
            Else
                MsgBox("Разделы: '" & text_dat & " / " & pages.Count & "' не объединены.", MsgBoxStyle.OkOnly, "Информация")
            End If
        Else
            MsgBox("Вы не выбрали разделы для объединения! Разделов: " & pages.Count, , "Ошибка обозревателя")
        End If
    End Sub
    Private Sub ОбъединитьПроэктыToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ОбъединитьПроэктыToolStripMenuItem.Click
        If Pages_View.SelectedIndices.Count = 1 Then
            Dim text_dat As String = Pages_View.SelectedIndices.Item(0) + 1
            Dim result As MsgBoxResult = MsgBox("Вы действительно хотите разделить раздел: '" & text_dat & " / " & pages.Count & "' ?", MsgBoxStyle.YesNo, "Удаление")
            If result = MsgBoxResult.Yes Then
                pages.Add(New Page)
                UpDateDisplay(0, 0)
                pages(pages.Count - 1).pages_settings(0) = "Безымянный"
                pages(pages.Count - 1).pages_settings(1) = "P"
                pages(pages.Count - 1).pages_settings(2) = 0
                pages(pages.Count - 1).pages_settings(3) = 0
                pages(pages.Count - 1).pages_settings(4) = 0
                pages(pages.Count - 1).pages_settings(5) = 1
                pages(pages.Count - 1).pages_settings(6) = 1
                pages(pages.Count - 1).pages_settings(7) = 1
                pages(pages.Count - 1).pages_settings(8) = ""
                pages(pages.Count - 1).pages_settings(9) = False
                Dim delenie_old As Integer = Math.Round(pages(pages_data).tracks.Count / 2)
                For i As Integer = delenie_old To pages(pages_data).tracks.Count - 1
                    pages(pages.Count - 1).tracks.Add(pages(pages_data).tracks(i))
                Next
                Dim deleted As Integer = 0
                For i As Integer = delenie_old To pages(pages_data).tracks.Count - 1
                    pages(pages_data).tracks.RemoveAt(i - deleted)
                    Tracks_View.Items.RemoveAt(i - deleted)
                    deleted += 1
                Next
                Renov_TracksView()
                Renov_PagesView()
                Pages_View.SelectedIndices.Clear()
                Pages_View.SelectedIndices.Add(pages_data)
                Pages_View.SelectedIndices.Add(pages.Count - 1)
            End If
        Else
            MsgBox("Вы не выбрали раздел для разделения! Одновремено разделять разделы нельзя! Разделов: " & pages.Count, , "Ошибка обозревателя")
        End If
    End Sub
    Public Sub Renov_PagesView()
        Pages_View.Items.Clear()
        For i As Integer = 0 To pages.Count - 1
            If i = pages_data Then
                Pages_View.Items.Add("--> " & i + 1 & " - Раздел: '" & pages(pages_data).pages_settings(0) & "', Тип раздела: " & pages(pages_data).pages_settings(1) & ", Треков: " & pages(pages_data).tracks.Count)
            Else
                Pages_View.Items.Add(i + 1 & " - Раздел: '" & pages(i).pages_settings(0) & "', Тип раздела: " & pages(i).pages_settings(1) & ", Треков: " & pages(i).tracks.Count)
            End If
        Next
    End Sub
    Public Sub Renov_TracksView()
        Tracks_View.Items.Clear()
        For i As Integer = 0 To pages(pages_data).tracks.Count - 1
            Tracks_View.Items.Add(i + 1 & " - " & Mid(pages(pages_data).tracks(i), pages(pages_data).tracks(i).LastIndexOf("\") + 2, pages(pages_data).tracks(i).Length - pages(pages_data).tracks(i).LastIndexOf("\") - 5))
        Next
    End Sub
    Private Sub ПоискToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ПоискToolStripMenuItem.Click
        Search.ShowDialog()
    End Sub
    Private Sub Button_Settings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Settings.Click
        Pages_Settings_Main.ShowDialog()
        UpDateDisplay(0, 0)
        Renov_PagesView()
    End Sub
    Private Sub ДобавитьРазделToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ДобавитьРазделToolStripMenuItem.Click
        pages.Add(New Page)
        UpDateDisplay(0, 0)
        pages(pages.Count - 1).pages_settings(0) = "Безымянный"
        pages(pages.Count - 1).pages_settings(1) = "P"
        pages(pages.Count - 1).pages_settings(2) = 0
        pages(pages.Count - 1).pages_settings(3) = 0
        pages(pages.Count - 1).pages_settings(4) = 0
        pages(pages.Count - 1).pages_settings(5) = 1
        pages(pages.Count - 1).pages_settings(6) = 1
        pages(pages.Count - 1).pages_settings(7) = 1
        pages(pages.Count - 1).pages_settings(8) = ""
        pages(pages.Count - 1).pages_settings(9) = False
        Pages_View.Items.Add(pages.Count & " - Раздел: '" & pages(pages.Count - 1).pages_settings(0) & "', Тип раздела: " & pages(pages.Count - 1).pages_settings(1) & ", Треков: " & pages(pages.Count - 1).tracks.Count)
    End Sub
    Private Sub УдалитьРазделToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles УдалитьРазделToolStripMenuItem.Click
        If Pages_View.SelectedIndices.Count > 0 Then
            Dim result As MsgBoxResult
            If Pages_View.SelectedIndices.Count > 1 Then
                Dim text_dat As String = ""
                For item As Integer = 0 To Pages_View.SelectedIndices.Count - 1
                    If item = 0 Then
                        text_dat = Pages_View.SelectedIndices.Item(item) + 1
                    Else
                        text_dat = text_dat & ", " & Pages_View.SelectedIndices.Item(item) + 1
                    End If
                Next
                result = MsgBox("Вы действительно хотите удалить разделы: '" & text_dat & " / " & pages.Count & "' ?", MsgBoxStyle.YesNo, "Удаление")
            Else
                result = MsgBox("Вы действительно хотите удалить раздел: '" & Pages_View.SelectedIndex + 1 & " / " & pages.Count & ", Имя раздела: '" & pages(Pages_View.SelectedIndex).pages_settings(0) & "', Тип раздела: " & pages(Pages_View.SelectedIndex).pages_settings(1) & ", Треков: " & pages(Pages_View.SelectedIndex).tracks.Count & "' ?", MsgBoxStyle.YesNo, "Удаление")
            End If
            Dim deleted As Integer = 0
            If result = MsgBoxResult.Yes Then
                Dim dat(Pages_View.SelectedIndices.Count - 1) As String
                For i As Integer = 0 To Pages_View.SelectedIndices.Count - 1
                    dat(i) = Pages_View.SelectedIndices(i)
                Next
                For i As Integer = 0 To dat.Length - 1
                    pages.RemoveAt(dat(i) - deleted)
                    deleted += 1
                Next
                If pages.Count = 0 Then
                    pages.Add(New Page)
                    UpDateDisplay(0, 0)
                    pages(pages.Count - 1).pages_settings(0) = "Безымянный"
                    pages(pages.Count - 1).pages_settings(1) = "P"
                    pages(pages.Count - 1).pages_settings(2) = 0
                    pages(pages.Count - 1).pages_settings(3) = 0
                    pages(pages.Count - 1).pages_settings(4) = 0
                    pages(pages.Count - 1).pages_settings(5) = 1
                    pages(pages.Count - 1).pages_settings(6) = 1
                    pages(pages.Count - 1).pages_settings(7) = 1
                    pages(pages.Count - 1).pages_settings(8) = ""
                    pages(pages.Count - 1).pages_settings(9) = False
                End If
                pages_data = 0
                Renov_PagesView()
                Renov_TracksView()
                UpDateDisplay(0, 0)
            End If
            MsgBox("Удалено разделов: " & deleted, , "Ошибка обозревателя")
        Else
            MsgBox("Вы не выбрали разделы для удаления! Разделов: " & pages.Count, , "Информация")
        End If
    End Sub
    Private Sub ОбъединитьРазделыToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ОбъединитьРазделыToolStripMenuItem.Click
        If Pages_View.SelectedIndices.Count >= 2 Then
            Dim text_dat As String = ""
            For item As Integer = 0 To Pages_View.SelectedIndices.Count - 1
                If item = 0 Then
                    text_dat = Pages_View.SelectedIndices.Item(item) + 1
                Else
                    text_dat = text_dat & ", " & Pages_View.SelectedIndices.Item(item) + 1
                End If
            Next
            Dim result As MsgBoxResult = MsgBox("Вы действительно хотите объединить разделы: '" & text_dat & " / " & pages.Count & "' ?", MsgBoxStyle.YesNo, "Объединение")
            If result = MsgBoxResult.Yes Then
                Dim dat(Pages_View.SelectedIndices.Count - 1) As String
                For i As Integer = 0 To Pages_View.SelectedIndices.Count - 1
                    dat(i) = Pages_View.SelectedIndices(i)
                Next
                For i As Integer = 1 To dat.Length - 1
                    For Each i1 As String In pages(dat(i)).tracks
                        pages(dat(0)).tracks.Add(i1)
                    Next
                Next
                Dim deleted As Integer = 0
                For i As Integer = 1 To dat.Length - 1
                    pages.RemoveAt(dat(i) - deleted)
                    Pages_View.Items.RemoveAt(dat(i) - deleted)
                    deleted += 1
                Next
                Renov_PagesView()
                Renov_TracksView()
                MsgBox("Разделы: '" & text_dat & " / " & pages.Count & "' объединены успешно.", MsgBoxStyle.OkOnly, "Информация")
            Else
                MsgBox("Разделы: '" & text_dat & " / " & pages.Count & "' не объединены.", MsgBoxStyle.OkOnly, "Информация")
            End If
        Else
            MsgBox("Вы не выбрали разделы для объединения! Разделов: " & pages.Count, , "Ошибка обозревателя")
        End If
    End Sub
    Private Sub РазделитьРазделToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles РазделитьРазделToolStripMenuItem.Click
        If Pages_View.SelectedIndices.Count = 1 Then
            Dim text_dat As String = Pages_View.SelectedIndices.Item(0) + 1
            Dim result As MsgBoxResult = MsgBox("Вы действительно хотите разделить раздел: '" & text_dat & " / " & pages.Count & "' ?", MsgBoxStyle.YesNo, "Удаление")
            If result = MsgBoxResult.Yes Then
                pages.Add(New Page)
                UpDateDisplay(0, 0)
                pages(pages.Count - 1).pages_settings(0) = "Безымянный"
                pages(pages.Count - 1).pages_settings(1) = "P"
                pages(pages.Count - 1).pages_settings(2) = 0
                pages(pages.Count - 1).pages_settings(3) = 0
                pages(pages.Count - 1).pages_settings(4) = 0
                pages(pages.Count - 1).pages_settings(5) = 1
                pages(pages.Count - 1).pages_settings(6) = 1
                pages(pages.Count - 1).pages_settings(7) = 1
                pages(pages.Count - 1).pages_settings(8) = ""
                pages(pages.Count - 1).pages_settings(9) = False
                Dim delenie_old As Integer = Math.Round(pages(pages_data).tracks.Count / 2)
                For i As Integer = delenie_old To pages(pages_data).tracks.Count - 1
                    pages(pages.Count - 1).tracks.Add(pages(pages_data).tracks(i))
                Next
                Dim deleted As Integer = 0
                For i As Integer = delenie_old To pages(pages_data).tracks.Count - 1
                    pages(pages_data).tracks.RemoveAt(i - deleted)
                    Tracks_View.Items.RemoveAt(i - deleted)
                    deleted += 1
                Next
                Renov_TracksView()
                Renov_PagesView()
                Pages_View.SelectedIndices.Clear()
                Pages_View.SelectedIndices.Add(pages_data)
                Pages_View.SelectedIndices.Add(pages.Count - 1)
            End If
        Else
            MsgBox("Вы не выбрали раздел для разделения! Одновремено разделять разделы нельзя! Разделов: " & pages.Count, , "Ошибка обозревателя")
        End If
    End Sub
    Private Sub Pages_View_MouseUp(sender As Object, e As MouseEventArgs) Handles Pages_View.MouseUp
        If Pages_View.SelectedIndices.Count = 1 Then
            Pages_View.Items.Item(pages_data) = pages_data + 1 & " - Раздел: '" & pages(pages_data).pages_settings(0) & "', Тип раздела: " & pages(pages_data).pages_settings(1) & ", Треков: " & pages(pages_data).tracks.Count
            pages_data = Pages_View.SelectedIndices(0)
            Pages_View.Items.Item(pages_data) = "--> " & pages_data + 1 & " - Раздел: '" & pages(pages_data).pages_settings(0) & "', Тип раздела: " & pages(pages_data).pages_settings(1) & ", Треков: " & pages(pages_data).tracks.Count
            Tracks_View.Items.Clear()
            For i As Integer = 0 To pages(Pages_View.SelectedIndices(0)).tracks.Count - 1
                Tracks_View.Items.Add(i + 1 & " - " & Mid(pages(pages_data).tracks(i), pages(pages_data).tracks(i).LastIndexOf("\") + 2, pages(pages_data).tracks(i).Length - pages(pages_data).tracks(i).LastIndexOf("\") - 5))
            Next
        End If
    End Sub
End Class
Public Class Page
    Public tracks As New ArrayList, pages_settings(9) As String
End Class
