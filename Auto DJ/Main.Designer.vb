﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Me.MainMenu = New System.Windows.Forms.MenuStrip()
        Me.ФайлToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.НовыйПроэктToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ОткрытьПроэктToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.СохранитьПроэктToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.СохранитьПроэктКакToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ПроэктDJToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ТекстовыйФайлToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ОбъединитьПроэктыToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.РазделитьПроэктToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ПроэктToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ОбзорСтраницToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.РазделитьПроэктыToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.УдалитьТрекиToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.УдалитьРаделToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ОбзорСтраницToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ОбъединитьПроэктыToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.ПоискToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ИнформацияToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ИнструментыToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.КопироватьЭлементыToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ВставитьЭлементыToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.ПереназначитьРазделPSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.ПереместитьТрекToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.НастройкиToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.НастройкиПрограммыToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.НастройкиПамятиToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.СToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.ЯзыкToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ЯзыкToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.СправкаToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ПомощьToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.ОСебеToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Button_Settings = New System.Windows.Forms.Button()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Button_Position_New = New System.Windows.Forms.Button()
        Me.Button_P_S = New System.Windows.Forms.Button()
        Me.Button_Paste = New System.Windows.Forms.Button()
        Me.Button_Copu = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PagesDisplay = New System.Windows.Forms.TextBox()
        Me.Menu_0 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ДобавитьРазделToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.УдалитьРазделToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator()
        Me.ОбъединитьРазделыToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.РазделитьРазделToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Tracks_View = New System.Windows.Forms.ListBox()
        Me.Menu_1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Pages_View = New System.Windows.Forms.ListBox()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Button_Next = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button_Reset = New System.Windows.Forms.Button()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Button_Stop = New System.Windows.Forms.Button()
        Me.Button_Play = New System.Windows.Forms.Button()
        Me.Button_Pause = New System.Windows.Forms.Button()
        Me.MainTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.ДобвитьТрекиToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.УдалитьТрекиToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator()
        Me.MainMenu.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Menu_0.SuspendLayout()
        Me.Menu_1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'MainMenu
        '
        Me.MainMenu.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.MainMenu.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.MainMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ФайлToolStripMenuItem, Me.ПроэктToolStripMenuItem, Me.ИнструментыToolStripMenuItem, Me.НастройкиToolStripMenuItem, Me.СправкаToolStripMenuItem})
        Me.MainMenu.Location = New System.Drawing.Point(0, 0)
        Me.MainMenu.Name = "MainMenu"
        Me.MainMenu.Size = New System.Drawing.Size(869, 25)
        Me.MainMenu.TabIndex = 0
        Me.MainMenu.Text = "MainMenu"
        '
        'ФайлToolStripMenuItem
        '
        Me.ФайлToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ФайлToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.НовыйПроэктToolStripMenuItem, Me.ОткрытьПроэктToolStripMenuItem, Me.СохранитьПроэктToolStripMenuItem, Me.СохранитьПроэктКакToolStripMenuItem, Me.ToolStripSeparator1, Me.ОбъединитьПроэктыToolStripMenuItem1, Me.РазделитьПроэктToolStripMenuItem})
        Me.ФайлToolStripMenuItem.Name = "ФайлToolStripMenuItem"
        Me.ФайлToolStripMenuItem.Size = New System.Drawing.Size(50, 21)
        Me.ФайлToolStripMenuItem.Text = "Файл"
        '
        'НовыйПроэктToolStripMenuItem
        '
        Me.НовыйПроэктToolStripMenuItem.Name = "НовыйПроэктToolStripMenuItem"
        Me.НовыйПроэктToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.НовыйПроэктToolStripMenuItem.Text = "Новый проэкт"
        '
        'ОткрытьПроэктToolStripMenuItem
        '
        Me.ОткрытьПроэктToolStripMenuItem.Name = "ОткрытьПроэктToolStripMenuItem"
        Me.ОткрытьПроэктToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.ОткрытьПроэктToolStripMenuItem.Text = "Открыть проэкт"
        '
        'СохранитьПроэктToolStripMenuItem
        '
        Me.СохранитьПроэктToolStripMenuItem.Name = "СохранитьПроэктToolStripMenuItem"
        Me.СохранитьПроэктToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.СохранитьПроэктToolStripMenuItem.Text = "Сохранить проэкт"
        '
        'СохранитьПроэктКакToolStripMenuItem
        '
        Me.СохранитьПроэктКакToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ПроэктDJToolStripMenuItem, Me.ТекстовыйФайлToolStripMenuItem})
        Me.СохранитьПроэктКакToolStripMenuItem.Name = "СохранитьПроэктКакToolStripMenuItem"
        Me.СохранитьПроэктКакToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.СохранитьПроэктКакToolStripMenuItem.Text = "Сохранить проэкт как"
        '
        'ПроэктDJToolStripMenuItem
        '
        Me.ПроэктDJToolStripMenuItem.Name = "ПроэктDJToolStripMenuItem"
        Me.ПроэктDJToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.ПроэктDJToolStripMenuItem.Text = "Проэкт DJ"
        '
        'ТекстовыйФайлToolStripMenuItem
        '
        Me.ТекстовыйФайлToolStripMenuItem.Name = "ТекстовыйФайлToolStripMenuItem"
        Me.ТекстовыйФайлToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.ТекстовыйФайлToolStripMenuItem.Text = "Текстовый файл"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(203, 6)
        '
        'ОбъединитьПроэктыToolStripMenuItem1
        '
        Me.ОбъединитьПроэктыToolStripMenuItem1.Name = "ОбъединитьПроэктыToolStripMenuItem1"
        Me.ОбъединитьПроэктыToolStripMenuItem1.Size = New System.Drawing.Size(206, 22)
        Me.ОбъединитьПроэктыToolStripMenuItem1.Text = "Объединить проэкты"
        '
        'РазделитьПроэктToolStripMenuItem
        '
        Me.РазделитьПроэктToolStripMenuItem.Name = "РазделитьПроэктToolStripMenuItem"
        Me.РазделитьПроэктToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.РазделитьПроэктToolStripMenuItem.Text = "Разделить проэкт"
        '
        'ПроэктToolStripMenuItem
        '
        Me.ПроэктToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ПроэктToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ОбзорСтраницToolStripMenuItem, Me.РазделитьПроэктыToolStripMenuItem, Me.УдалитьТрекиToolStripMenuItem, Me.УдалитьРаделToolStripMenuItem, Me.ToolStripSeparator2, Me.ОбзорСтраницToolStripMenuItem1, Me.ОбъединитьПроэктыToolStripMenuItem, Me.ToolStripSeparator4, Me.ПоискToolStripMenuItem, Me.ToolStripSeparator5, Me.ИнформацияToolStripMenuItem})
        Me.ПроэктToolStripMenuItem.Name = "ПроэктToolStripMenuItem"
        Me.ПроэктToolStripMenuItem.Size = New System.Drawing.Size(62, 21)
        Me.ПроэктToolStripMenuItem.Text = "Проэкт"
        '
        'ОбзорСтраницToolStripMenuItem
        '
        Me.ОбзорСтраницToolStripMenuItem.Name = "ОбзорСтраницToolStripMenuItem"
        Me.ОбзорСтраницToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.ОбзорСтраницToolStripMenuItem.Text = "Добавить треки"
        Me.ОбзорСтраницToolStripMenuItem.ToolTipText = "Добавляет треки в раздел соответственно 3 - м режимам."
        '
        'РазделитьПроэктыToolStripMenuItem
        '
        Me.РазделитьПроэктыToolStripMenuItem.Name = "РазделитьПроэктыToolStripMenuItem"
        Me.РазделитьПроэктыToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.РазделитьПроэктыToolStripMenuItem.Text = "Добавить раздел"
        Me.РазделитьПроэктыToolStripMenuItem.ToolTipText = "Добавляет раздел."
        '
        'УдалитьТрекиToolStripMenuItem
        '
        Me.УдалитьТрекиToolStripMenuItem.Name = "УдалитьТрекиToolStripMenuItem"
        Me.УдалитьТрекиToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.УдалитьТрекиToolStripMenuItem.Text = "Удалить треки"
        Me.УдалитьТрекиToolStripMenuItem.ToolTipText = "Удаляет треки из раздела."
        '
        'УдалитьРаделToolStripMenuItem
        '
        Me.УдалитьРаделToolStripMenuItem.Name = "УдалитьРаделToolStripMenuItem"
        Me.УдалитьРаделToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.УдалитьРаделToolStripMenuItem.Text = "Удалить разделы"
        Me.УдалитьРаделToolStripMenuItem.ToolTipText = "Удаляет разделы."
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(201, 6)
        '
        'ОбзорСтраницToolStripMenuItem1
        '
        Me.ОбзорСтраницToolStripMenuItem1.Name = "ОбзорСтраницToolStripMenuItem1"
        Me.ОбзорСтраницToolStripMenuItem1.Size = New System.Drawing.Size(204, 22)
        Me.ОбзорСтраницToolStripMenuItem1.Text = "Объединить разделы"
        Me.ОбзорСтраницToolStripMenuItem1.ToolTipText = "Объединяет 2 и больше разделов в один." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Чтобы объединить разделы: выделите их и н" &
    "ажмите эту кнопку."
        '
        'ОбъединитьПроэктыToolStripMenuItem
        '
        Me.ОбъединитьПроэктыToolStripMenuItem.Name = "ОбъединитьПроэктыToolStripMenuItem"
        Me.ОбъединитьПроэктыToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.ОбъединитьПроэктыToolStripMenuItem.Text = "Разделить раздел"
        Me.ОбъединитьПроэктыToolStripMenuItem.ToolTipText = "Разделяет выбранный раздел на 2. Треки в разделе тоже делятся."
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(201, 6)
        '
        'ПоискToolStripMenuItem
        '
        Me.ПоискToolStripMenuItem.Name = "ПоискToolStripMenuItem"
        Me.ПоискToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.ПоискToolStripMenuItem.Text = "Поиск"
        Me.ПоискToolStripMenuItem.ToolTipText = "Поиск трека или раздела."
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(201, 6)
        '
        'ИнформацияToolStripMenuItem
        '
        Me.ИнформацияToolStripMenuItem.Name = "ИнформацияToolStripMenuItem"
        Me.ИнформацияToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.ИнформацияToolStripMenuItem.Text = "О проэкте"
        Me.ИнформацияToolStripMenuItem.ToolTipText = "Информация о проэкте."
        '
        'ИнструментыToolStripMenuItem
        '
        Me.ИнструментыToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.КопироватьЭлементыToolStripMenuItem, Me.ВставитьЭлементыToolStripMenuItem, Me.ToolStripSeparator6, Me.ПереназначитьРазделPSToolStripMenuItem, Me.ToolStripSeparator7, Me.ПереместитьТрекToolStripMenuItem})
        Me.ИнструментыToolStripMenuItem.Name = "ИнструментыToolStripMenuItem"
        Me.ИнструментыToolStripMenuItem.Size = New System.Drawing.Size(99, 21)
        Me.ИнструментыToolStripMenuItem.Text = "Инструменты"
        '
        'КопироватьЭлементыToolStripMenuItem
        '
        Me.КопироватьЭлементыToolStripMenuItem.Name = "КопироватьЭлементыToolStripMenuItem"
        Me.КопироватьЭлементыToolStripMenuItem.Size = New System.Drawing.Size(264, 22)
        Me.КопироватьЭлементыToolStripMenuItem.Text = "Копировать элемент(ы)"
        '
        'ВставитьЭлементыToolStripMenuItem
        '
        Me.ВставитьЭлементыToolStripMenuItem.Name = "ВставитьЭлементыToolStripMenuItem"
        Me.ВставитьЭлементыToolStripMenuItem.Size = New System.Drawing.Size(264, 22)
        Me.ВставитьЭлементыToolStripMenuItem.Text = "Вставить элемент(ы)"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(261, 6)
        '
        'ПереназначитьРазделPSToolStripMenuItem
        '
        Me.ПереназначитьРазделPSToolStripMenuItem.Name = "ПереназначитьРазделPSToolStripMenuItem"
        Me.ПереназначитьРазделPSToolStripMenuItem.Size = New System.Drawing.Size(264, 22)
        Me.ПереназначитьРазделPSToolStripMenuItem.Text = "Изменить тип раздела(ов) (P/S)"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(261, 6)
        '
        'ПереместитьТрекToolStripMenuItem
        '
        Me.ПереместитьТрекToolStripMenuItem.Name = "ПереместитьТрекToolStripMenuItem"
        Me.ПереместитьТрекToolStripMenuItem.Size = New System.Drawing.Size(264, 22)
        Me.ПереместитьТрекToolStripMenuItem.Text = "Переместить трек(и)"
        '
        'НастройкиToolStripMenuItem
        '
        Me.НастройкиToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.НастройкиToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.НастройкиПрограммыToolStripMenuItem, Me.ToolStripSeparator9, Me.ЯзыкToolStripMenuItem, Me.ЯзыкToolStripMenuItem1})
        Me.НастройкиToolStripMenuItem.Name = "НастройкиToolStripMenuItem"
        Me.НастройкиToolStripMenuItem.Size = New System.Drawing.Size(83, 21)
        Me.НастройкиToolStripMenuItem.Text = "Настройки"
        '
        'НастройкиПрограммыToolStripMenuItem
        '
        Me.НастройкиПрограммыToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.НастройкиПамятиToolStripMenuItem, Me.СToolStripMenuItem})
        Me.НастройкиПрограммыToolStripMenuItem.Name = "НастройкиПрограммыToolStripMenuItem"
        Me.НастройкиПрограммыToolStripMenuItem.Size = New System.Drawing.Size(224, 22)
        Me.НастройкиПрограммыToolStripMenuItem.Text = "Расширенные настройки"
        '
        'НастройкиПамятиToolStripMenuItem
        '
        Me.НастройкиПамятиToolStripMenuItem.Name = "НастройкиПамятиToolStripMenuItem"
        Me.НастройкиПамятиToolStripMenuItem.Size = New System.Drawing.Size(185, 22)
        Me.НастройкиПамятиToolStripMenuItem.Text = "Настройки памяти"
        Me.НастройкиПамятиToolStripMenuItem.ToolTipText = "Настройки памяти настроек."
        '
        'СToolStripMenuItem
        '
        Me.СToolStripMenuItem.Name = "СToolStripMenuItem"
        Me.СToolStripMenuItem.Size = New System.Drawing.Size(185, 22)
        Me.СToolStripMenuItem.Text = "Настройки"
        Me.СToolStripMenuItem.ToolTipText = "Открыть окно настроек."
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(221, 6)
        '
        'ЯзыкToolStripMenuItem
        '
        Me.ЯзыкToolStripMenuItem.Name = "ЯзыкToolStripMenuItem"
        Me.ЯзыкToolStripMenuItem.Size = New System.Drawing.Size(224, 22)
        Me.ЯзыкToolStripMenuItem.Text = "Стандартные настройки"
        Me.ЯзыкToolStripMenuItem.ToolTipText = "Открыть окно стандартных настроек."
        '
        'ЯзыкToolStripMenuItem1
        '
        Me.ЯзыкToolStripMenuItem1.Name = "ЯзыкToolStripMenuItem1"
        Me.ЯзыкToolStripMenuItem1.Size = New System.Drawing.Size(224, 22)
        Me.ЯзыкToolStripMenuItem1.Text = "Язык"
        Me.ЯзыкToolStripMenuItem1.ToolTipText = "Выбрать язык."
        '
        'СправкаToolStripMenuItem
        '
        Me.СправкаToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.СправкаToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ПомощьToolStripMenuItem, Me.ToolStripSeparator8, Me.ОСебеToolStripMenuItem})
        Me.СправкаToolStripMenuItem.Name = "СправкаToolStripMenuItem"
        Me.СправкаToolStripMenuItem.Size = New System.Drawing.Size(70, 21)
        Me.СправкаToolStripMenuItem.Text = "Справка"
        '
        'ПомощьToolStripMenuItem
        '
        Me.ПомощьToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ПомощьToolStripMenuItem.Name = "ПомощьToolStripMenuItem"
        Me.ПомощьToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.ПомощьToolStripMenuItem.Text = "Помощь"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(155, 6)
        '
        'ОСебеToolStripMenuItem
        '
        Me.ОСебеToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ОСебеToolStripMenuItem.Name = "ОСебеToolStripMenuItem"
        Me.ОСебеToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.ОСебеToolStripMenuItem.Text = "О программе"
        '
        'Button_Settings
        '
        Me.Button_Settings.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button_Settings.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Button_Settings.Location = New System.Drawing.Point(687, 415)
        Me.Button_Settings.Name = "Button_Settings"
        Me.Button_Settings.Size = New System.Drawing.Size(179, 84)
        Me.Button_Settings.TabIndex = 4
        Me.Button_Settings.Text = "Настройки раздела"
        Me.MainTip.SetToolTip(Me.Button_Settings, "Открыть окно настроек выбранного раздела.")
        Me.Button_Settings.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.Button_Position_New, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.Button_P_S, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Button_Paste, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Button_Copu, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel2, 0, 4)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(687, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 5
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(179, 403)
        Me.TableLayoutPanel2.TabIndex = 1
        '
        'Button_Position_New
        '
        Me.Button_Position_New.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button_Position_New.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Button_Position_New.Location = New System.Drawing.Point(3, 258)
        Me.Button_Position_New.Name = "Button_Position_New"
        Me.Button_Position_New.Size = New System.Drawing.Size(173, 79)
        Me.Button_Position_New.TabIndex = 3
        Me.Button_Position_New.Text = "Переместить трек(и)"
        Me.Button_Position_New.UseVisualStyleBackColor = True
        '
        'Button_P_S
        '
        Me.Button_P_S.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button_P_S.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Button_P_S.Location = New System.Drawing.Point(3, 173)
        Me.Button_P_S.Name = "Button_P_S"
        Me.Button_P_S.Size = New System.Drawing.Size(173, 79)
        Me.Button_P_S.TabIndex = 2
        Me.Button_P_S.Text = "Переназначить раздел (P/S)"
        Me.MainTip.SetToolTip(Me.Button_P_S, "Задать разделу режим воспроизведения (P - порядковый, S -  случайный).")
        Me.Button_P_S.UseVisualStyleBackColor = True
        '
        'Button_Paste
        '
        Me.Button_Paste.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button_Paste.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Button_Paste.Location = New System.Drawing.Point(3, 88)
        Me.Button_Paste.Name = "Button_Paste"
        Me.Button_Paste.Size = New System.Drawing.Size(173, 79)
        Me.Button_Paste.TabIndex = 1
        Me.Button_Paste.Text = "Вставить элемент(ы)"
        Me.MainTip.SetToolTip(Me.Button_Paste, "Вставить выделенный(е) элемент(ы).")
        Me.Button_Paste.UseVisualStyleBackColor = True
        '
        'Button_Copu
        '
        Me.Button_Copu.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button_Copu.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Button_Copu.Location = New System.Drawing.Point(3, 3)
        Me.Button_Copu.Name = "Button_Copu"
        Me.Button_Copu.Size = New System.Drawing.Size(173, 79)
        Me.Button_Copu.TabIndex = 0
        Me.Button_Copu.Text = "Копировать элемент(ы)"
        Me.MainTip.SetToolTip(Me.Button_Copu, "Копировать выделенный(е) элемент(ы). Скопированный(е) элемент(ы) можно вставить н" &
        "ажатием кнопки ""Вставить элемент(ы)""")
        Me.Button_Copu.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.PagesDisplay)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 343)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(173, 57)
        Me.Panel2.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 20)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Тип: P"
        '
        'PagesDisplay
        '
        Me.PagesDisplay.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PagesDisplay.BackColor = System.Drawing.SystemColors.ControlLight
        Me.PagesDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PagesDisplay.ContextMenuStrip = Me.Menu_0
        Me.PagesDisplay.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.PagesDisplay.Location = New System.Drawing.Point(3, 28)
        Me.PagesDisplay.Name = "PagesDisplay"
        Me.PagesDisplay.ReadOnly = True
        Me.PagesDisplay.Size = New System.Drawing.Size(164, 26)
        Me.PagesDisplay.TabIndex = 1
        Me.PagesDisplay.Text = "Раздел: 1 / 1"
        Me.MainTip.SetToolTip(Me.PagesDisplay, "Следующий трек. (Переход совершается автоматически.)")
        '
        'Menu_0
        '
        Me.Menu_0.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Menu_0.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ДобавитьРазделToolStripMenuItem, Me.ToolStripSeparator3, Me.УдалитьРазделToolStripMenuItem, Me.ToolStripSeparator10, Me.ОбъединитьРазделыToolStripMenuItem, Me.РазделитьРазделToolStripMenuItem})
        Me.Menu_0.Name = "Menu_0"
        Me.Menu_0.Size = New System.Drawing.Size(205, 104)
        '
        'ДобавитьРазделToolStripMenuItem
        '
        Me.ДобавитьРазделToolStripMenuItem.Name = "ДобавитьРазделToolStripMenuItem"
        Me.ДобавитьРазделToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.ДобавитьРазделToolStripMenuItem.Text = "Добавить раздел"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(201, 6)
        '
        'УдалитьРазделToolStripMenuItem
        '
        Me.УдалитьРазделToolStripMenuItem.Name = "УдалитьРазделToolStripMenuItem"
        Me.УдалитьРазделToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.УдалитьРазделToolStripMenuItem.Text = "Удалить раздел"
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(201, 6)
        '
        'ОбъединитьРазделыToolStripMenuItem
        '
        Me.ОбъединитьРазделыToolStripMenuItem.Name = "ОбъединитьРазделыToolStripMenuItem"
        Me.ОбъединитьРазделыToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.ОбъединитьРазделыToolStripMenuItem.Text = "Объединить разделы"
        '
        'РазделитьРазделToolStripMenuItem
        '
        Me.РазделитьРазделToolStripMenuItem.Name = "РазделитьРазделToolStripMenuItem"
        Me.РазделитьРазделToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.РазделитьРазделToolStripMenuItem.Text = "Разделить раздел"
        '
        'Tracks_View
        '
        Me.Tracks_View.ContextMenuStrip = Me.Menu_1
        Me.Tracks_View.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tracks_View.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Tracks_View.FormattingEnabled = True
        Me.Tracks_View.HorizontalScrollbar = True
        Me.Tracks_View.ItemHeight = 20
        Me.Tracks_View.Location = New System.Drawing.Point(0, 0)
        Me.Tracks_View.Name = "Tracks_View"
        Me.Tracks_View.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.Tracks_View.Size = New System.Drawing.Size(478, 406)
        Me.Tracks_View.TabIndex = 0
        Me.MainTip.SetToolTip(Me.Tracks_View, "Здесь отображаются ваши треки.")
        '
        'Menu_1
        '
        Me.Menu_1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Menu_1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ДобвитьТрекиToolStripMenuItem, Me.ToolStripSeparator11, Me.УдалитьТрекиToolStripMenuItem1})
        Me.Menu_1.Name = "Menu_0"
        Me.Menu_1.Size = New System.Drawing.Size(165, 54)
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 185.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.SplitContainer1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Button_Settings, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel3, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 25)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 110.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(869, 522)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.Pages_View)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.Tracks_View)
        Me.SplitContainer1.Size = New System.Drawing.Size(678, 406)
        Me.SplitContainer1.SplitterDistance = 196
        Me.SplitContainer1.TabIndex = 2
        '
        'Pages_View
        '
        Me.Pages_View.ContextMenuStrip = Me.Menu_0
        Me.Pages_View.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Pages_View.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Pages_View.FormattingEnabled = True
        Me.Pages_View.HorizontalScrollbar = True
        Me.Pages_View.ItemHeight = 20
        Me.Pages_View.Location = New System.Drawing.Point(0, 0)
        Me.Pages_View.Name = "Pages_View"
        Me.Pages_View.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.Pages_View.Size = New System.Drawing.Size(196, 406)
        Me.Pages_View.TabIndex = 1
        Me.MainTip.SetToolTip(Me.Pages_View, "Здесь отображаются ваши разделы.")
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Button_Next, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Button_Play, 1, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.TableLayoutPanel4, 0, 1)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 415)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 2
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(678, 104)
        Me.TableLayoutPanel3.TabIndex = 5
        '
        'Button_Next
        '
        Me.Button_Next.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button_Next.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Button_Next.Location = New System.Drawing.Point(477, 3)
        Me.Button_Next.Name = "Button_Next"
        Me.Button_Next.Size = New System.Drawing.Size(198, 53)
        Me.Button_Next.TabIndex = 5
        Me.Button_Next.Text = "Далее >>"
        Me.MainTip.SetToolTip(Me.Button_Next, "Перейти к следующему треку. (Высвечивается в поле ""Следующий трек"".)")
        Me.Button_Next.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.TextBox)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(468, 56)
        Me.Panel1.TabIndex = 6
        '
        'TextBox
        '
        Me.TextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.TextBox.Location = New System.Drawing.Point(0, 14)
        Me.TextBox.Name = "TextBox"
        Me.TextBox.ReadOnly = True
        Me.TextBox.Size = New System.Drawing.Size(468, 26)
        Me.TextBox.TabIndex = 0
        Me.MainTip.SetToolTip(Me.TextBox, "Следующий трек. (Переход совершается автоматически.)")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label1.Location = New System.Drawing.Point(-3, -3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 16)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Следующий трек:"
        '
        'Button_Reset
        '
        Me.Button_Reset.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button_Reset.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Button_Reset.Location = New System.Drawing.Point(3, 3)
        Me.Button_Reset.Name = "Button_Reset"
        Me.Button_Reset.Size = New System.Drawing.Size(148, 30)
        Me.Button_Reset.TabIndex = 7
        Me.Button_Reset.Text = "Сброс"
        Me.MainTip.SetToolTip(Me.Button_Reset, "Сброс воспроизведения.")
        Me.Button_Reset.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 3
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.Button_Reset, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Button_Pause, 2, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Button_Stop, 1, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(3, 65)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(468, 36)
        Me.TableLayoutPanel4.TabIndex = 8
        '
        'Button_Stop
        '
        Me.Button_Stop.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button_Stop.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Button_Stop.Location = New System.Drawing.Point(157, 3)
        Me.Button_Stop.Name = "Button_Stop"
        Me.Button_Stop.Size = New System.Drawing.Size(148, 30)
        Me.Button_Stop.TabIndex = 10
        Me.Button_Stop.Text = "Стоп"
        Me.MainTip.SetToolTip(Me.Button_Stop, "Остановить воспроизведение. (Воспроизводимая на момент остановки композиция, при " &
        "нажатии кнопки ""Пуск"", будет воспроизводится с начала.)")
        Me.Button_Stop.UseVisualStyleBackColor = True
        '
        'Button_Play
        '
        Me.Button_Play.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button_Play.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Button_Play.Location = New System.Drawing.Point(477, 65)
        Me.Button_Play.Name = "Button_Play"
        Me.Button_Play.Size = New System.Drawing.Size(198, 36)
        Me.Button_Play.TabIndex = 8
        Me.Button_Play.Text = "Пуск"
        Me.MainTip.SetToolTip(Me.Button_Play, "Начать воспроизведение.")
        Me.Button_Play.UseVisualStyleBackColor = True
        '
        'Button_Pause
        '
        Me.Button_Pause.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button_Pause.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Button_Pause.Location = New System.Drawing.Point(311, 3)
        Me.Button_Pause.Name = "Button_Pause"
        Me.Button_Pause.Size = New System.Drawing.Size(154, 30)
        Me.Button_Pause.TabIndex = 9
        Me.Button_Pause.Text = "Пауза"
        Me.MainTip.SetToolTip(Me.Button_Pause, "Приостановить воспроизведение.")
        Me.Button_Pause.UseVisualStyleBackColor = True
        '
        'ДобвитьТрекиToolStripMenuItem
        '
        Me.ДобвитьТрекиToolStripMenuItem.Name = "ДобвитьТрекиToolStripMenuItem"
        Me.ДобвитьТрекиToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.ДобвитьТрекиToolStripMenuItem.Text = "Добвить треки"
        '
        'УдалитьТрекиToolStripMenuItem1
        '
        Me.УдалитьТрекиToolStripMenuItem1.Name = "УдалитьТрекиToolStripMenuItem1"
        Me.УдалитьТрекиToolStripMenuItem1.Size = New System.Drawing.Size(164, 22)
        Me.УдалитьТрекиToolStripMenuItem1.Text = "Удалить треки"
        '
        'ToolStripSeparator11
        '
        Me.ToolStripSeparator11.Name = "ToolStripSeparator11"
        Me.ToolStripSeparator11.Size = New System.Drawing.Size(161, 6)
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ClientSize = New System.Drawing.Size(869, 547)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.MainMenu)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MainMenu
        Me.MinimumSize = New System.Drawing.Size(756, 584)
        Me.Name = "Main"
        Me.Text = "Auto DJ pro --[Новый проэкт.dj]--"
        Me.MainMenu.ResumeLayout(False)
        Me.MainMenu.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Menu_0.ResumeLayout(False)
        Me.Menu_1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MainMenu As System.Windows.Forms.MenuStrip
    Friend WithEvents ФайлToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents НовыйПроэктToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ОткрытьПроэктToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents СохранитьПроэктToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents СохранитьПроэктКакToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ПроэктDJToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ТекстовыйФайлToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ПроэктToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ОбзорСтраницToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents РазделитьПроэктыToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ОбзорСтраницToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents НастройкиToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents НастройкиПрограммыToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents НастройкиПамятиToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents СToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ЯзыкToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ЯзыкToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents СправкаToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ПомощьToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ОСебеToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ОбъединитьПроэктыToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button_Settings As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Button_Position_New As System.Windows.Forms.Button
    Friend WithEvents Button_P_S As System.Windows.Forms.Button
    Friend WithEvents Button_Paste As System.Windows.Forms.Button
    Friend WithEvents Button_Copu As System.Windows.Forms.Button
    Friend WithEvents Tracks_View As System.Windows.Forms.ListBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Button_Next As System.Windows.Forms.Button
    Friend WithEvents TextBox As System.Windows.Forms.TextBox
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button_Reset As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Button_Stop As System.Windows.Forms.Button
    Friend WithEvents Button_Play As System.Windows.Forms.Button
    Friend WithEvents Button_Pause As System.Windows.Forms.Button
    Friend WithEvents MainTip As System.Windows.Forms.ToolTip
    Friend WithEvents УдалитьТрекиToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents УдалитьРаделToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PagesDisplay As System.Windows.Forms.TextBox
    Friend WithEvents Menu_0 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ИнформацияToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ОбъединитьПроэктыToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents РазделитьПроэктToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ПоискToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ИнструментыToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents КопироватьЭлементыToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ВставитьЭлементыToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ПереназначитьРазделPSToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ПереместитьТрекToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents ДобавитьРазделToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
    Friend WithEvents УдалитьРазделToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator10 As ToolStripSeparator
    Friend WithEvents ОбъединитьРазделыToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents РазделитьРазделToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Menu_1 As ContextMenuStrip
    Friend WithEvents Pages_View As ListBox
    Friend WithEvents ДобвитьТрекиToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator11 As ToolStripSeparator
    Friend WithEvents УдалитьТрекиToolStripMenuItem1 As ToolStripMenuItem
End Class
