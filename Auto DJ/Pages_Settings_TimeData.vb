﻿Public Class Pages_Settings_TimeData
    Private Sub Pages_Settings_TimeData_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Main.pages(Main.pages_data).pages_settings(8) = "" Then
            CheckBox1.Checked = False
            TextBox1.Enabled = False
            TextBox2.Enabled = False
            TextBox3.Enabled = False
            TextBox4.Enabled = False
            TextBox5.Enabled = False
            Button3.Enabled = False
        Else
            CheckBox1.Checked = True
            TextBox1.Enabled = True
            TextBox2.Enabled = True
            TextBox3.Enabled = True
            TextBox4.Enabled = True
            TextBox5.Enabled = True
            Button3.Enabled = True
            TextBox1.Text = DateValue(Main.pages(Main.pages_data).pages_settings(8)).Minute
            TextBox2.Text = DateValue(Main.pages(Main.pages_data).pages_settings(8)).Hour
            TextBox3.Text = DateValue(Main.pages(Main.pages_data).pages_settings(8)).Month
            TextBox4.Text = DateValue(Main.pages(Main.pages_data).pages_settings(8)).Month
            TextBox5.Text = DateValue(Main.pages(Main.pages_data).pages_settings(8)).Year
        End If
        If Main.pages(Main.pages_data).pages_settings(9) = False Then
            Button3.Text = "Исключить из воспроизведения"
            button_state = False
        Else
            Button3.Text = "Включить в воспроизведение"
            button_state = True
        End If
    End Sub
    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            TextBox1.Enabled = True
            TextBox2.Enabled = True
            TextBox3.Enabled = True
            TextBox4.Enabled = True
            TextBox5.Enabled = True
            Button3.Enabled = True
        Else
            TextBox1.Enabled = False
            TextBox2.Enabled = False
            TextBox3.Enabled = False
            TextBox4.Enabled = False
            TextBox5.Enabled = False
            Button3.Enabled = False
        End If
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If CheckBox1.Checked = True Then
            Try
                Dim dat As Date
                With dat
                    .AddMinutes(CType(TextBox1.Text, Integer))
                    .AddHours(CType(TextBox2.Text, Integer))
                    .AddDays(CType(TextBox3.Text, Integer))
                    .AddMonths(CType(TextBox4.Text, Integer))
                    .AddYears(CType(TextBox5.Text, Integer))
                End With
                Main.pages(Main.pages_data).pages_settings(8) = dat
                Main.pages(Main.pages_data).pages_settings(9) = button_state
                Me.Close()
            Catch
                MsgBox("Неверно указана дата!", , "Ошибка Настройки")
            End Try
        Else
            Main.pages(Main.pages_data).pages_settings(8) = ""
            Me.Close()
        End If
    End Sub
    Dim button_state As Boolean
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If button_state = False Then
            Button3.Text = "Включить в воспроизведение"
            button_state = True
        Else
            Button3.Text = "Исключить из воспроизведения"
            button_state = False
        End If
    End Sub
End Class
