﻿Public Class Search
    Private Sub Search_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Button2.Enabled = True
        CheckBox1.Enabled = True
        ComboBox1.Enabled = True
        Page_Type.Enabled = True
        Track_name.Enabled = True
        Page_name.Enabled = True
        Page_number.Enabled = True
        Tracks_inPage.Enabled = True
    End Sub
    Private Sub Search_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If ComboBox1.SelectedIndex = 0 Then Track_name.Enabled = True
        Track_name.Text = ""
        Page_name.Text = ""
        Page_number.Text = ""
        Tracks_inPage.Text = ""
        ComboBox1.SelectedIndex = 0
        Page_Type.SelectedIndex = 0
    End Sub
    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If ComboBox1.SelectedIndex = 0 Then
            Track_name.Enabled = True
            Label8.Text = "Поиск: 0/5"
        Else
            Track_name.Enabled = False
            Label8.Text = "Поиск: 0/4"
        End If
        If ComboBox1.SelectedIndex = 0 Then
            If Page_Type.SelectedIndex = 0 And Track_name.Text = "" And Page_name.Text = "" And Page_number.Text = "" And Tracks_inPage.Text = "" Then
                Button2.Enabled = False
                CheckBox1.Enabled = False
            Else
                Button2.Enabled = True
                CheckBox1.Enabled = True
            End If
        Else
            If Page_Type.SelectedIndex = 0 And Page_name.Text = "" And Page_number.Text = "" And Tracks_inPage.Text = "" Then
                Button2.Enabled = False
                CheckBox1.Enabled = False
            Else
                Button2.Enabled = True
                CheckBox1.Enabled = True
            End If
        End If
    End Sub
    Dim faze As Short = 18
    Private Sub Searching_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Searching.Tick
        If Search_Work.IsBusy = False Then
            faze = 1
            Label7.Text = ""
            Searching.Stop()
        End If
        Label7.Text = Mid("            > > > >", faze, 12)
        If faze = 1 Then
            faze = 18
        Else
            faze -= 1
        End If
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Button2.Enabled = False
        CheckBox1.Enabled = False
        ComboBox1.Enabled = False
        Page_Type.Enabled = False
        Track_name.Enabled = False
        Page_name.Enabled = False
        Page_number.Enabled = False
        Tracks_inPage.Enabled = False
        Searching.Start()
        Search_Work.RunWorkerAsync()
    End Sub
    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Page_Type.SelectedIndexChanged
        If ComboBox1.SelectedIndex = 0 Then
            If Page_Type.SelectedIndex = 0 And Track_name.Text = "" And Page_name.Text = "" And Page_number.Text = "" And Tracks_inPage.Text = "" Then
                Button2.Enabled = False
                CheckBox1.Enabled = False
            Else
                Button2.Enabled = True
                CheckBox1.Enabled = True
            End If
        Else
            If Page_Type.SelectedIndex = 0 And Page_name.Text = "" And Page_number.Text = "" And Tracks_inPage.Text = "" Then
                Button2.Enabled = False
                CheckBox1.Enabled = False
            Else
                Button2.Enabled = True
                CheckBox1.Enabled = True
            End If
        End If
    End Sub
    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Track_name.TextChanged
        If ComboBox1.SelectedIndex = 0 Then
            If Page_Type.SelectedIndex = 0 And Track_name.Text = "" And Page_name.Text = "" And Page_number.Text = "" And Tracks_inPage.Text = "" Then
                Button2.Enabled = False
                CheckBox1.Enabled = False
            Else
                Button2.Enabled = True
                CheckBox1.Enabled = True
            End If
        Else
            If Page_Type.SelectedIndex = 0 And Page_name.Text = "" And Page_number.Text = "" And Tracks_inPage.Text = "" Then
                Button2.Enabled = False
                CheckBox1.Enabled = False
            Else
                Button2.Enabled = True
                CheckBox1.Enabled = True
            End If
        End If
    End Sub
    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Page_name.TextChanged
        If ComboBox1.SelectedIndex = 0 Then
            If Page_Type.SelectedIndex = 0 And Track_name.Text = "" And Page_name.Text = "" And Page_number.Text = "" And Tracks_inPage.Text = "" Then
                Button2.Enabled = False
                CheckBox1.Enabled = False
            Else
                Button2.Enabled = True
                CheckBox1.Enabled = True
            End If
        Else
            If Page_Type.SelectedIndex = 0 And Page_name.Text = "" And Page_number.Text = "" And Tracks_inPage.Text = "" Then
                Button2.Enabled = False
                CheckBox1.Enabled = False
            Else
                Button2.Enabled = True
                CheckBox1.Enabled = True
            End If
        End If
    End Sub
    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Page_number.TextChanged
        If ComboBox1.SelectedIndex = 0 Then
            If Page_Type.SelectedIndex = 0 And Track_name.Text = "" And Page_name.Text = "" And Page_number.Text = "" And Tracks_inPage.Text = "" Then
                Button2.Enabled = False
                CheckBox1.Enabled = False
            Else
                Button2.Enabled = True
                CheckBox1.Enabled = True
            End If
        Else
            If Page_Type.SelectedIndex = 0 And Page_name.Text = "" And Page_number.Text = "" And Tracks_inPage.Text = "" Then
                Button2.Enabled = False
                CheckBox1.Enabled = False
            Else
                Button2.Enabled = True
                CheckBox1.Enabled = True
            End If
        End If
    End Sub
    Private Sub TextBox4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Tracks_inPage.TextChanged
        If ComboBox1.SelectedIndex = 0 Then
            If Page_Type.SelectedIndex = 0 And Track_name.Text = "" And Page_name.Text = "" And Page_number.Text = "" And Tracks_inPage.Text = "" Then
                Button2.Enabled = False
                CheckBox1.Enabled = False
            Else
                Button2.Enabled = True
                CheckBox1.Enabled = True
            End If
        Else
            If Page_Type.SelectedIndex = 0 And Page_name.Text = "" And Page_number.Text = "" And Tracks_inPage.Text = "" Then
                Button2.Enabled = False
                CheckBox1.Enabled = False
            Else
                Button2.Enabled = True
                CheckBox1.Enabled = True
            End If
        End If
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Searching.Stop()
        Me.Close()
    End Sub
    Private Sub Search_Work_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles Search_Work.DoWork
        Dim res As New ArrayList
        If ComboBox1.SelectedIndex = 0 Then
            For i As Integer = 0 To Main.pages.Count - 1
                res.Add(i)
            Next
            If Page_number.Text <> "" Then
                Try
                    Dim number_page As Integer = Page_number.Text
                    If Main.pages.Count < number_page And number_page > 0 Then
                        res.Clear()
                        res.Add(number_page - 1)
                    Else
                        MsgBox("Номер раздела больше колличества разделов или равен 0!", , "Ошибка Поиск")
                        Button2.Enabled = True
                        CheckBox1.Enabled = True
                        ComboBox1.Enabled = True
                        Page_Type.Enabled = True
                        Page_name.Enabled = True
                        Page_number.Enabled = True
                        Tracks_inPage.Enabled = True
                        faze = 1
                        Label7.Text = ""
                        Searching.Stop()
                        Exit Sub
                    End If
                Catch
                    MsgBox("Неверно задан номер раздела!", , "Ошибка Поиск")
                    Button2.Enabled = True
                    CheckBox1.Enabled = True
                    ComboBox1.Enabled = True
                    Page_Type.Enabled = True
                    Page_name.Enabled = True
                    Page_number.Enabled = True
                    Tracks_inPage.Enabled = True
                    faze = 1
                    Label7.Text = ""
                    Searching.Stop()
                    Exit Sub
                End Try
            End If
            Label8.Text = "Поиск: 1/5"
            If Page_Type.SelectedIndex <> 0 Then
                Dim dat As New ArrayList
                For i As Integer = 0 To res.Count - 1
                    If Page_Type.SelectedItem = "P" And Main.pages(res(i)).pages_settings(1) <> "P" Then
                        dat.Add(res(i))
                    ElseIf Page_Type.SelectedItem = "S" And Main.pages(res(i)).pages_settings(1) <> "S" Then
                        dat.Add(res(i))
                    End If
                Next
                Dim deleted As Integer = 0
                For i As Integer = 0 To dat.Count - 1
                    res.RemoveAt(dat(i) - deleted)
                    deleted += 1
                Next
            End If
            Label8.Text = "Поиск: 2/5"
            If Tracks_inPage.Text <> "" Then
                Try
                    Dim number_tracks As Short = Tracks_inPage.Text
                    Dim dat As New ArrayList
                    For i As Integer = 0 To res.Count - 1
                        If Main.pages(res(i)).tracks.Count <> number_tracks Then
                            dat.Add(res(i))
                        End If
                    Next
                    Dim deleted As Integer = 0
                    For i As Integer = 0 To dat.Count - 1
                        res.RemoveAt(dat(i) - deleted)
                        deleted += 1
                    Next
                Catch
                    MsgBox("Неверно задано колличество треков в разделе!", , "Ошибка Поиск")
                    Button2.Enabled = True
                    CheckBox1.Enabled = True
                    ComboBox1.Enabled = True
                    Page_Type.Enabled = True
                    Page_name.Enabled = True
                    Page_number.Enabled = True
                    Tracks_inPage.Enabled = True
                    faze = 1
                    Label7.Text = ""
                    Searching.Stop()
                    Exit Sub
                End Try
            End If
            Label8.Text = "Поиск: 3/5"
            If Page_name.Text <> "" Then
                Dim dat As New ArrayList
                If CheckBox1.Checked Then
                    For i As Integer = 0 To res.Count - 1
                        If Main.pages(res(i)).pages_settings(0) <> Page_name.Text Then
                            dat.Add(res(i))
                        End If
                    Next
                Else
                    For i As Integer = 0 To res.Count - 1
                        If Main.pages(res(i)).pages_settings(0).IndexOf(Page_name.Text) = -1 Then
                            dat.Add(res(i))
                        End If
                    Next
                End If
                Dim deleted As Integer = 0
                For i As Integer = 0 To dat.Count - 1
                    res.RemoveAt(dat(i) - deleted)
                    deleted += 1
                Next
            End If
            Label8.Text = "Поиск: 4/5"
            Dim tracks_dat As New ArrayList
            If Track_name.Text <> "" Then
                For i As Integer = 0 To res.Count - 1
                    For i1 As Integer = 0 To Main.pages(res(i)).tracks.Count
                        If CheckBox1.Checked Then
                            If Main.pages(res(i)).tracks(i1) = Track_name.Text Then
                                tracks_dat.Add(i & "=" & i1)
                            End If
                        Else
                            If Main.pages(res(i)).tracks(i1).ToLower.IndexOf(Track_name.Text.ToLower) >= 0 Then
                                tracks_dat.Add(i & "=" & i1)
                            End If
                        End If
                    Next
                Next
            End If
            Label8.Text = "Поиск: 5/5"
            If (res.Count > 0 And Track_name.Text <> "") Or (tracks_dat.Count > 0 And Track_name.Text <> "") Then
                Main.Pages_View.SelectedIndices.Clear()
                For Each i As Integer In res
                    Main.Pages_View.SelectedIndices.Add(i)
                Next
                If Track_name.Text = "" Then
                    MsgBox("Поиск завершён успешно! К параметрам поиска подходят все треки в указанных разделах. Найдено разделов: " & res.Count, , "Сообщение")
                    Button2.Enabled = True
                    CheckBox1.Enabled = True
                    ComboBox1.Enabled = True
                    Page_Type.Enabled = True
                    Track_name.Enabled = True
                    Page_name.Enabled = True
                    Page_number.Enabled = True
                    Tracks_inPage.Enabled = True
                Else
                    Dim result As String = "", dat As String = ""
                    For i As Integer = 0 To tracks_dat.Count - 1
                        If dat = "" Or tracks_dat(i).Split("=")(0) = dat Then
                            result = Chr(10) & "    Номер раздел: " & tracks_dat(i).Split("=")(0) + 1 & " Номер трека: " & tracks_dat(i).Split("=")(1) + 1
                            dat = tracks_dat(i).Split("=")(0)
                        Else
                            result &= ", " & tracks_dat(i).Split("=")(1) + 1
                        End If
                    Next
                    MsgBox("Поиск завершён успешно! Найдено треков: " & tracks_dat.Count & " Местоположение треков: " & result, , "Сообщение")
                    Button2.Enabled = True
                    CheckBox1.Enabled = True
                    ComboBox1.Enabled = True
                    Page_Type.Enabled = True
                    Track_name.Enabled = True
                    Page_name.Enabled = True
                    Page_number.Enabled = True
                    Tracks_inPage.Enabled = True
                End If
            Else
                Dim result_search(1) As String
                If CheckBox1.Checked Then
                    result_search(0) = "    Снимите галочку в поле 'Данные точны'" & Chr(10)
                Else
                    result_search(0) = ""
                End If
                If Page_number.Text <> "" Or Tracks_inPage.Text <> "" Or Page_Type.SelectedIndex > 0 Then
                    result_search(1) = "    Попробуйте не использовать источники точных параметров (это поля 'Номер раздела', 'Треков в разделе', 'Имя разделя' и выбор Типа раздела)." & Chr(10)
                Else
                    result_search(1) = ""
                End If
                MsgBox("Поиск не нашёл треков, соответствующих заданным параметрам!" & Chr(10) & "Для улучшения результата поиска попробуйте следующее:" & Chr(10) & result_search(0) & result_search(1) & "    Проверьте правильность введённого имени трека.", , "Сообщение")
                Button2.Enabled = True
                CheckBox1.Enabled = True
                ComboBox1.Enabled = True
                Page_Type.Enabled = True
                Track_name.Enabled = True
                Page_name.Enabled = True
                Page_number.Enabled = True
                Tracks_inPage.Enabled = True
            End If
        Else
            For i As Integer = 0 To Main.pages.Count - 1
                res.Add(i)
            Next
            If Page_number.Text <> "" Then
                Try
                    Dim number_page As Integer = Page_number.Text
                    If Main.pages.Count < number_page And number_page > 0 Then
                        res.Clear()
                        res.Add(number_page - 1)
                    Else
                        MsgBox("Номер раздела больше колличества разделов или равен 0!", , "Ошибка Поиск")
                        Button2.Enabled = True
                        CheckBox1.Enabled = True
                        ComboBox1.Enabled = True
                        Page_Type.Enabled = True
                        Page_name.Enabled = True
                        Page_number.Enabled = True
                        Tracks_inPage.Enabled = True
                        faze = 1
                        Label7.Text = ""
                        Searching.Stop()
                        Exit Sub
                    End If
                Catch
                    MsgBox("Неверно задан номер раздела!", , "Ошибка Поиск")
                    Button2.Enabled = True
                    CheckBox1.Enabled = True
                    ComboBox1.Enabled = True
                    Page_Type.Enabled = True
                    Page_name.Enabled = True
                    Page_number.Enabled = True
                    Tracks_inPage.Enabled = True
                    faze = 1
                    Label7.Text = ""
                    Searching.Stop()
                    Exit Sub
                End Try
            End If
            Label8.Text = "Поиск: 1/4"
            If Page_Type.SelectedIndex <> 0 Then
                Dim dat As New ArrayList
                For i As Integer = 0 To res.Count - 1
                    If Page_Type.SelectedItem = "P" And Main.pages(res(i)).pages_settings(1) <> "P" Then
                        dat.Add(res(i))
                    ElseIf Page_Type.SelectedItem = "S" And Main.pages(res(i)).pages_settings(1) <> "S" Then
                        dat.Add(res(i))
                    End If
                Next
                Dim deleted As Integer = 0
                For i As Integer = 0 To dat.Count - 1
                    res.RemoveAt(dat(i) - deleted)
                    deleted += 1
                Next
            End If
            Label8.Text = "Поиск: 2/4"
            If Tracks_inPage.Text <> "" Then
                Try
                    Dim number_tracks As Short = Tracks_inPage.Text
                    Dim dat As New ArrayList
                    For i As Integer = 0 To res.Count - 1
                        If Main.pages(res(i)).tracks.Count <> number_tracks Then
                            dat.Add(res(i))
                        End If
                    Next
                    Dim deleted As Integer = 0
                    For i As Integer = 0 To dat.Count - 1
                        res.RemoveAt(dat(i) - deleted)
                        deleted += 1
                    Next
                Catch
                    MsgBox("Неверно задано колличество треков в разделе!", , "Ошибка Поиск")
                    Button2.Enabled = True
                    CheckBox1.Enabled = True
                    ComboBox1.Enabled = True
                    Page_Type.Enabled = True
                    Page_name.Enabled = True
                    Page_number.Enabled = True
                    Tracks_inPage.Enabled = True
                    faze = 1
                    Label7.Text = ""
                    Searching.Stop()
                    Exit Sub
                End Try
            End If
            Label8.Text = "Поиск: 3/4"
            If Page_name.Text <> "" Then
                Dim dat As New ArrayList
                If CheckBox1.Checked Then
                    For i As Integer = 0 To res.Count - 1
                        If Main.pages(res(i)).pages_settings(0) <> Page_name.Text Then
                            dat.Add(res(i))
                        End If
                    Next
                Else
                    For i As Integer = 0 To res.Count - 1
                        If Main.pages(res(i)).pages_settings(0).ToLower.IndexOf(Page_name.Text.ToLower) = -1 Then
                            dat.Add(res(i))
                        End If
                    Next
                End If
                Dim deleted As Integer = 0
                For i As Integer = 0 To dat.Count - 1
                    res.RemoveAt(dat(i) - deleted)
                    deleted += 1
                Next
            End If
            Label8.Text = "Поиск: 4/4"
            If res.Count > 0 Then
                Main.Pages_View.SelectedIndices.Clear()
                For Each i As Integer In res
                    Main.Pages_View.SelectedIndices.Add(res(i))
                Next
                MsgBox("Поиск завершён успешно! Найдено разделов: " & res.Count, , "Сообщение")
                Button2.Enabled = True
                CheckBox1.Enabled = True
                ComboBox1.Enabled = True
                Page_Type.Enabled = True
                Page_name.Enabled = True
                Page_number.Enabled = True
                Tracks_inPage.Enabled = True
            Else
                Dim result_search(1) As String
                If CheckBox1.Checked Then
                    result_search(0) = "    Снимите галочку в поле 'Данные точны'" & Chr(10)
                Else
                    result_search(0) = ""
                End If
                If Page_number.Text <> "" Or Tracks_inPage.Text <> "" Or Page_Type.SelectedIndex > 0 Then
                    result_search(1) = "    Попробуйте не использовать источники точных параметров (это поля 'Номер раздела', 'Треков в разделе' и выбор Типа раздела)." & Chr(10)
                Else
                    result_search(1) = ""
                End If
                MsgBox("Поиск не нашёл разделов, соответствующих заданным параметрам!" & Chr(10) & "Для улучшения результата поиска попробуйте следующее:" & Chr(10) & result_search(0) & result_search(1) & "    Проверьте правильность введённого имени раздела.", , "Сообщение")
                Button2.Enabled = True
                CheckBox1.Enabled = True
                ComboBox1.Enabled = True
                Page_Type.Enabled = True
                Page_name.Enabled = True
                Page_number.Enabled = True
                Tracks_inPage.Enabled = True
            End If
        End If
    End Sub
End Class
